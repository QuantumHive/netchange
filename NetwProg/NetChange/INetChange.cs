﻿using NetwProg.Networking;
using System.Collections.Generic;

namespace NetwProg.NetChange
{
    /// <summary>
    /// Represents an interface for the NetChange algorithm.
    /// </summary>
    interface INetChange
    {
        void Init();
        void Recompute(int destination);
        void Repair(Client newNode);
        void Failure(Client oldNode);

        /// <summary>
        /// All known nodes in the network.
        /// </summary>
        IList<int> Network { get; set; }
        /// <summary>
        /// Size of the network.
        /// </summary>
        int NetworkSize { get; }
        /// <summary>
        /// The direct neighbors for this node u 'Neigh_u'.
        /// [neighbours portnumber, the outgoing socket to that neighbour]
        /// </summary>
        IList<Client> Neighbours { get; set; }
        /// <summary>
        /// DistanceTable 'D_u[v]'.
        /// [node in the network, number of estimated hops to that node]
        /// </summary>
        IDictionary<int, int> DistanceTable { get; set; }
        /// <summary>
        /// Nb_u[v] is preferred neighbor for v.
        /// [node in the network, the fastest path to that node]
        /// </summary>
        IDictionary<int, Client> RoutingTable { get; set; }
        /// <summary>
        /// ndis[w,v] is a list of estimated distances from every neighbour to every node in the network
        /// </summary>
        IDictionary<Distance, int> Ndis { get; set; }
    }
}