﻿//Note: Distance needs to be a struct, because we use this as a key in a IDictionary.
//If Distance was a class, we could not easily use it to reference a key in the dictionary.
namespace NetwProg.NetChange
{
    /// <summary>
    /// Represents a key to hold in a IDictionary to know the distance from a node to it's destination.
    /// This is used for 'ndis' in the NetChange algorithm.
    /// </summary>
    struct Distance
    {
        public int Node, Destination;

        /// <summary>
        /// Used for ndis
        /// </summary>
        /// <param name="node">Your neighbour.</param>
        /// <param name="destination">Destination for the neighbour.</param>
        public Distance(int node, int destination)
        {
            this.Node = node;
            this.Destination = destination;
        }
    }
}