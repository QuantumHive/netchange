﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NetwProg.Resources;
using NetwProg.Networking;

namespace NetwProg.NetChange
{
    //remarks on this class: doing ToList() on IEnumerables in a foreach loop creates a copy, so no need to lock

    /// <summary>
    /// An implementation of the NetChange algorithm guided by Tajibnapis' Netchange algorithm from the book/pdf.
    /// </summary>
    class NetChangeService : INetChange
    {
        private readonly Client local;

        public NetChangeService()
        {
            this.Network = new List<int>();
            this.Neighbours = new List<Client>();
            this.DistanceTable = new Dictionary<int, int>();
            this.RoutingTable = new Dictionary<int, Client>();
            this.Ndis = new Dictionary<Distance, int>();

            this.local = Client.Local();
        }

        public void Init()
        {
            Distance ndis_wv;

            foreach (var neighbour in this.Neighbours.ToList())
            {
                foreach (var node in this.Network.ToList())
                {
                    ndis_wv = new Distance(neighbour.Node, node);
                    lock (NetwProg.MyLock) 
                    { 
                        this.Ndis.Add(ndis_wv, this.NetworkSize); 
                    }
                }
            }

            foreach (var node in this.Network.ToList())
            {
                lock (NetwProg.MyLock)
                {
                    this.DistanceTable.Add(node, this.NetworkSize);
                    this.RoutingTable.Add(node, null); //null = undefined
                }
            }
            
            lock (NetwProg.MyLock)
            {
                DistanceTable[NetwProg.LocalNode] = 0; //distance to this node is 0
                RoutingTable[NetwProg.LocalNode] = local; //routing to this node is our own connection to this node
            }

            foreach (var client in Neighbours.ToList())
            {
                this.SendMydist(client, this.local.Node);
            }

            NetwProg.Initializing = false;
        }

        public void Recompute(int destination)
        {
            bool hasChanged = false;
            int distance = 0;
            if (destination == this.local.Node)
            {
                hasChanged = DistanceTable[destination] != 0;
                lock (NetwProg.MyLock)
                {
                    DistanceTable[destination] = 0;
                    RoutingTable[destination] = this.local;
                }
            }
            else
            {
                /*Estimate distance to destination*/
                Distance w = Min(destination); //ndis[w,v]: get the d(w,v) of our neighbour w with lowest distance to v
                distance = w.Node == 0 && w.Destination == 0 ? NetworkSize : 1 + Ndis[w];
                if (distance < NetworkSize)
                {
                    hasChanged = DistanceTable[destination] != distance;
                    lock (NetwProg.MyLock)
                    {
                        DistanceTable[destination] = distance;
                        RoutingTable[destination] = Neighbours.FirstOrDefault(c => c.Node == w.Node);
                    }
                }
                else
                {
                    try
                    {
                        hasChanged = DistanceTable[destination] != NetworkSize;
                        lock (NetwProg.MyLock)
                        {
                            DistanceTable[destination] = NetworkSize;
                            RoutingTable[destination] = null; //null = undefined
                        }
                    }
                    catch { }
                }
            }
            if (hasChanged)
            {
                try
                {
                    Logger.Output(OutputResources.distance, destination, distance, RoutingTable[destination].Node);
                }
                catch //client in routingtable is null
                {
                    lock (NetwProg.MyLock)
                    {
                        this.RoutingTable.Remove(destination);
                    }
                }
                foreach (var neighbour in this.Neighbours.ToList())
                {
                    try
                    {
                        SendMydist(neighbour, destination);
                    }
                    catch
                    {
                        Failure(neighbour);
                    }
                }
            }
            NetwProg.Recomputing = 0; //done recomputing
        }

        public void Repair(Client newNode)
        {
            foreach (int node in Network.ToList())
            {
                try
                {
                    lock (NetwProg.MyLock)
                    {
                        this.Ndis.Add(new Distance(newNode.Node, node), NetworkSize);
                    }
                }
                catch { }
                SendMydist(this.Neighbours.First(c => c.Node == newNode.Node), node);
            }
        }

        public void Failure(Client oldNode)
        {
            try
            {
                lock (NetwProg.MyLock)
                {
                    this.Neighbours.Remove(oldNode);
                }
            }
            catch { }
            foreach (int node in this.Network.ToList())
            {
                this.Recompute(node);
            }
        }

        private void SendMydist(Client neighbour, int destination)
        {
            neighbour.SendMessage("@ {0} {1} ", destination, this.DistanceTable[destination]); //<mydist, v, Du[v]>
        }

        private Distance Min(int destination)
        {
            int min = NetworkSize;
            Distance ndis_wv = new Distance(0, 0); //dummy struct with 0 values
            lock (NetwProg.MyLock)
            {
                foreach (var neighbour in this.Neighbours)
                    foreach (Distance dis in this.Ndis.Keys)
                        if (dis.Node == neighbour.Node && dis.Destination == destination && Ndis[dis] < min)
                        {

                            {
                                min = Ndis[dis];
                                ndis_wv = dis;
                            }
                        }
            }
            return ndis_wv;
        }

        #region Properties
        public IList<int> Network { get; set; }
        public int NetworkSize { get { return this.Network.Count; } }
        public IList<Client> Neighbours { get; set; }
        public IDictionary<int, int> DistanceTable { get; set; }
        public IDictionary<int, Client> RoutingTable { get; set; }
        public IDictionary<Distance, int> Ndis { get; set; }
        #endregion
    }
}