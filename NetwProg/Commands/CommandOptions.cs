﻿using System.Linq;
using System.Text;
using NetwProg.Resources;
using NetwProg.Networking;

namespace NetwProg.Commands
{
    /// <summary>
    /// This class implements the commands, which are targeted by grammar rule attributes.
    /// These are custom commands required for the assignment.
    /// Use with CommandLineParser class.
    /// </summary>
    class CommandOptions
    {
        private CommandOptions() { }

        /// <summary>
        /// Prints the RoutingTable Nb_u
        /// </summary>
        [VerbCommand("R")]
        public void ShowRoutingtable()
        {
            Logger.Output("{0} {1} {2}",
                NetwProg.LocalNode,
                0,
                "local");

            NetwProg.NetChangeInstance.Network
                .Where<int>(n => n != NetwProg.LocalNode)
                .ToList<int>()
                .ForEach(node =>
                    {
                        try
                        {
                            lock (NetwProg.MyLock)
                            {
                                Logger.Output("{0} {1} {2}",
                                node,
                                NetwProg.NetChangeInstance.DistanceTable[node],
                                NetwProg.NetChangeInstance.RoutingTable[node].Node);
                            }
                        }
                        catch { }
                    }
                );
        }

        /// <summary>
        /// Sends a message in the network.
        /// </summary>
        /// <param name="msg">First element in the array is expected to be the destination. All other elements are part of the message which are divided with spaces.</param>
        [VerbCommand("B")]
        public void SendMessage(string[] msg)
        {
            int destination = int.Parse(msg[0]);
            string theMessage = msg.Skip(1).Aggregate((current, next) => string.Format("{0} {1}", current, next));
            if (destination == NetwProg.LocalNode)
            {
                Logger.Output(OutputResources.forward_message, destination, "local");
                Logger.Output(theMessage);
            }
            else
            {
                StringBuilder mail = new StringBuilder();
                mail.Append(string.Format("{0} ", "B")); //add command
                mail.Append(string.Format("{0} ", NetwProg.LocalNode)); //add source
                mail.Append(string.Format("{0} ", destination)); //add destination
                mail.Append(string.Format("{0} ", msg.Length)); //add the length of the message
                mail.Append(string.Format("{0} ", theMessage)); //add the message

                try
                {
                    NetwProg.NetChangeInstance.RoutingTable[destination].SendMessage(mail.ToString()); //mail: "B [source] [destination] [messagelength] [message]"
                    Logger.Output(OutputResources.forward_message, destination, NetwProg.NetChangeInstance.RoutingTable[destination].Node);
                }
                catch
                {
                    Logger.Output(OutputResources.unknown, destination);
                }
            }
        }

        /// <summary>
        /// Will try to start a new Client which will connect to a node in the network.
        /// </summary>
        /// <param name="msg">First element of the array should be a string representation of a node.</param>
        [VerbCommand("C")]
        public void Connect(string[] msg)
        {
            int node = int.Parse(msg.First());

            if (node == NetwProg.LocalNode) 
            {
                //quote from the assignment ;)
                Logger.Debug("Quote: 'We zullen nooit een process met zichzelf laten verbinden.'");
            }
            else if (NetwProg.NetChangeInstance.Neighbours.FirstOrDefault(c => c.Node == node) == null)
            {
                Client.Start(node);
            }
        }

        /// <summary>
        /// Will try to disconnect with the node in the network.
        /// </summary>
        /// <param name="msg">First element of the array should be a string representation of a node.</param>
        [VerbCommand("D")]
        public void Disconnect(string[] msg)
        {
            int node = int.Parse(msg.First());

            if (node == NetwProg.LocalNode)
            {
                Logger.Debug(". . . *facepalm*");
            }
            else
            {
                try
                {
                    var disconnect = NetwProg.NetChangeInstance.Neighbours.FirstOrDefault(c => c.Node == node);
                    disconnect.Dispose();
                    NetwProg.NetChangeInstance.Failure(disconnect);
                    Logger.Output(OutputResources.disconnected, node);
                } catch { }
            }
        }

        private static CommandOptions _instance;
        /// <summary>
        /// Gets one instance of this class.
        /// </summary>
        public static CommandOptions Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CommandOptions();
                }
                return _instance;
            }
        }
    }
}
