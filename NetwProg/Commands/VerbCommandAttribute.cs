﻿using System;

/*
 * Nice to have:
 * - Specify parameter types, numbers and order
 * - Long names
 * - Regex for types and/or misspelling
 */
namespace NetwProg.Commands
{

    /// <summary>
    /// Attribute which targets only methods to specify a verb command from the command line.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class VerbCommandAttribute : Attribute
    {
        /// <summary>
        /// Specify a verb command.
        /// </summary>
        /// <param name="shortName">A short name for the verb command.</param>
        public VerbCommandAttribute(string shortName)
        {
            this.ShortName = shortName;
        }

        /// <summary>
        /// Gets the short name of the verb command.
        /// </summary>
        public string ShortName { get; private set; }
    }
}