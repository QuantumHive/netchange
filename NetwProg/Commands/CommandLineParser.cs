﻿/*
 * Custom Command Line Parser inspired by the official Command Line Parser on Github/NuGet.
 * Copyright © 2014 Alper Aslan (3869768). All rights reserved.
 */

/*
 * Note by Alper:
 * This entire functionality could be pretty much implemented in a faster and easier way (e.g. looping over a switch/case).
 * However, I wanted to practice using attributes with .NET's Reflection.
 * So that's why I wanted to implement this with the inspiration of the NuGet package for the official Command Line Parser.
 * See: https://github.com/gsscoder/commandline
 */
using System;
using System.Linq;
using System.Reflection;

namespace NetwProg.Commands
{
    class CommandLineParser
    {
        private readonly Type info;
        private readonly object commandOptions;

        private CommandLineParser() { }
        private CommandLineParser(object commandOptions)
        {
            this.commandOptions = commandOptions;
            this.info = this.commandOptions.GetType();
        }

        private bool TryInvokeMethod()
        {
            bool invoked = false;
            //methods
            foreach (var method in this.info.GetMethods())
            {
                foreach (VerbCommandAttribute verbCommand in method.GetCustomAttributes(typeof(VerbCommandAttribute)))
                {
                    if (verbCommand.ShortName.ToUpper().Equals(this.VerbCommand))
                    {
                        try
                        {
                            method.Invoke(this.commandOptions, this.Arguments);
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e, "Could not invoke method for command '{0}'", verbCommand.ShortName);
                            return false;
                        }
                        finally
                        {
                            invoked = true;
                        }
                    }
                }
                if (invoked) break;
            }
            if (!invoked)
            {
                Logger.Debug("No such command '{0}'", this.VerbCommand);
            }
            return true;
        }

        private string _verb;
        private string VerbCommand 
        { 
            get { return this._verb.ToUpper(); }
            set { this._verb = value.ToUpper(); } 
        }
        private object[] Arguments { get; set; }

        /// <summary>
        /// Parses a string of command line arguments and executes that command.
        /// </summary>
        /// <param name="commandOptions">Class with methods that are targeted with appropriate attributes to define grammar rules.</param>
        /// <param name="command">A string with commands and optional arguments</param>
        /// <returns>True if no errors occurred, else false.</returns>
        public static bool ParseExecute(object commandOptions, string command)
        {
            var parser = new CommandLineParser(commandOptions);

            parser.VerbCommand = command
                .Split()
                .FirstOrDefault();

            var args = command
                .Split()
                .Skip(1);
            
            parser.Arguments = args.Count() > 0 ? new[] { args.ToArray() } : null;

            return parser.VerbCommand != null && parser.TryInvokeMethod();
        }
    }
}
