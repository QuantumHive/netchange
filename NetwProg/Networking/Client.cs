﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using NetwProg.Resources;
using System.Threading;
using NetwProg.NetChange;
using System.IO;

namespace NetwProg.Networking
{
    /// <summary>
    /// Client class which handles protocols for the NetChange algorithm and the requirements of the assignment.
    /// </summary>
    class Client : IDisposable
    {
        private readonly TcpClient client;
        private bool disposed = false;

        private Client()
        {
            this.Node = NetwProg.LocalNode;
            this.client = null;
        }
        private Client(int connectionPort)
        {
            this.Node = connectionPort;
            this.client = new TcpClient();
            new Thread(this.Connect).Start();
        }
        private Client(Socket socket)
        {
            this.client = new TcpClient();
            this.client.Client = socket;
            new Thread(this.IncomingConnection).Start();
        }

        /// <summary>
        /// Method that handles an incoming connection started by the server.
        /// A handshake is made to determine the remote node number and the connection will be registered.
        /// An "OK" message will be send to notify the remote node all went well on our side.
        /// After that we start up the Receiving thread (listenloop).
        /// </summary>
        private void IncomingConnection()
        {
            //get the remote node number
            //(bufferlength=5 because you guys said ports are between: 1-65535)
            byte[] buffer = new byte[5];
            this.client.Client.Receive(buffer, 5, SocketFlags.None);
            
            //client sent his node number
            this.Node = int.Parse(Encoding.ASCII.GetString(buffer, 0, 5).Replace("\0", String.Empty));
            
            //sync with handshake
            this.client.Client.Send(Encoding.ASCII.GetBytes("OK"), 2, 0);

            //register
            lock (NetwProg.MyLock)
            {
                NetwProg.NetChangeInstance.Neighbours.Add(this);
            }

            //start receiving messages
            new Thread(this.ListenLoop).Start();
        }

        /// <summary>
        /// Connect method, that connects to a remote node in the network.
        /// Assuming that the remote node listens to incoming connections.
        /// When connected we send our node number and register the connection.
        /// If things went well on the other side, we will receive an "OK" message so that we can start up the Receiving thread (listenloop).
        /// </summary>
        private void Connect()
        {
            byte attempt = 1;
        reconnect:
            try
            {
                this.client.Connect(IPAddress.Loopback, this.Node); //attempting to connect with remote
            }
            catch
            {
                if (attempt < 255) //retry?
                {
                    attempt++; 
                    goto reconnect;
                }
                else //give up.
                {
                    NetwProg.ConnectionCount--;
                    this.Dispose();
                    return;
                }
            }

            //connected
            
            this.SendMessage(NetwProg.LocalNode.ToString()); //send our node number

            //sync with handshake
            byte[] OK = new byte[2];
            client.Client.Receive(OK, 2, 0);

            //register
            lock (NetwProg.MyLock)
            {
                NetwProg.NetChangeInstance.Neighbours.Add(this);
            }

            //start receiving messages
            new Thread(this.ListenLoop).Start();

            //for the 'C [node]' command, initiliazing is done after all initial connects, so we know this is true whenever we can give user input
            if (!NetwProg.Initializing)
            {
                NetwProg.NetChangeInstance.Repair(this);
                this.SendMessage("C");
            }
        }

        /// <summary>
        /// Receiver method, gets fired for every node after a connection has been established to receive messages from it's neighbour.
        /// Every node has one thread for every neighbour it is connected to.
        /// The receiving methods starts receiving messages after NetChange's Init() method is done.
        /// This method loops over every incoming data from the socket. This message is parsed and checked on the commands in it.
        /// These can be multiple commands received in one message, which are handled correctly.
        /// </summary>
        private void ListenLoop()
        {
            string msg = string.Empty; //total message
            string cmd = string.Empty; //command
            string[] msgs = new string[0]; //messages split by spaces
            int source = 0; //source node that used the 'B' command
            int destination = 0; //destination in 'B' command
            int distance = 0; //distance in mydist
            Distance ndis_wv = new Distance(0, 0); 
            int x = 0; //variable used for multiple messages in one receive method

            while (NetwProg.Initializing)
            {
                Task.Delay(1000); //wait until NetChange's Init() is done
            }
            
            Logger.Output(OutputResources.connected, this.Node);            
            
            while (true) //loop
            {
                try
                {
                    msg = this.ReceiveMessage(); //wait for incoming messages
                }
                catch (IOException) //disconnected
                {
                    if (!this.disposed)
                    {
                        this.Dispose();
                        NetwProg.NetChangeInstance.Failure(this);
                        Logger.Output(OutputResources.disconnected, this.Node);
                    }
                    return;
                }
                catch (IndexOutOfRangeException) //empty message
                {
                    continue;
                }

                msgs = msg.Split();

                //loops over multiple messages if there are any
                for (int i = 0; i < msgs.Length; i += x)
                {
                    cmd = msgs[i].ToUpper(); //note the use of i

                    switch (cmd)
                    {
                        case "C":
                            NetwProg.NetChangeInstance.Repair(this);
                            x = 2; //because connections are of type 'C node'
                            break;
                        case "B": //B [source] [destination] [messagelength] [message]
                            //we use i here to look up the location of our message, we add some numbers, because we know the protocol format of our custom 'B' command
                            source = int.Parse(msg.Split()[1 + i]);
                            destination = int.Parse(msg.Split()[2 + i]);
                            x = int.Parse(msg.Split()[3 + i]) + 4;

                            if (destination == NetwProg.LocalNode)  //message for us
                            {
                                Logger.Output(string.Join(" ", msgs, i + 4, x - 4));
                            }
                            else if (NetwProg.NetChangeInstance.Network.Contains(destination)) //forward message
                            {
                                NetwProg.NetChangeInstance.RoutingTable[destination].SendMessage(string.Join(" ", msgs, i, x));
                                Logger.Output(OutputResources.forward_message, destination, NetwProg.NetChangeInstance.RoutingTable[destination].Node);
                            }
                            else //node must have left the network..
                            {
                                Logger.Output(OutputResources.unreachable, destination);
                            }
                            break;
                        case "@": //<mydist, destination, distance>
                            //we use "@" in our protocol to define a mydist message
                            //again note the use of i
                            destination = int.Parse(msgs[i + 1]);
                            distance = int.Parse(msgs[i + 2]);
                            ndis_wv = new Distance(this.Node, destination);

                            while (Interlocked.Exchange(ref NetwProg.Recomputing, 1) == 1)
                            {
                                Task.Delay(200);   //wait untill some other thread is done recomputing [ATOMIC/THREADSAFE]
                            }

                            if (NetwProg.NetChangeInstance.Ndis.ContainsKey(ndis_wv))
                            {
                                lock (NetwProg.MyLock)
                                {
                                    NetwProg.NetChangeInstance.Ndis[ndis_wv] = distance; //update ndis of my neighbour
                                }
                            }
                            else
                            {
                                lock (NetwProg.MyLock)
                                {
                                    NetwProg.NetChangeInstance.Ndis.Add(ndis_wv, distance); //add ndis of my neighbour
                                }
                                //expand network if destination is unknown
                                if (!NetwProg.NetChangeInstance.Network.Contains(destination))
                                { 
                                    lock (NetwProg.MyLock)
                                    {
                                        NetwProg.NetChangeInstance.Network.Add(destination);
                                        NetwProg.NetChangeInstance.DistanceTable.Add(destination, NetwProg.NetChangeInstance.NetworkSize);
                                        NetwProg.NetChangeInstance.RoutingTable.Add(destination, null); //null = undefined
                                    }
                                }
                            }
                            
                            NetwProg.NetChangeInstance.Recompute(destination);

                            x = 3; //because my dist is like '@ destination distance'
                            break;
                    }
                }
            }
        }

        #region Network I/O
        /// <summary>
        /// Sends a message to our direct neighbour.
        /// </summary>
        /// <param name="message">A composite format string.</param>
        /// <param name="args">The object to format.</param>
        public void SendMessage(string message, params object[] args)
        {
            this.client.Client.Send(Encoding.ASCII.GetBytes(string.Format(message, args)));
        }

        /// <summary>
        /// Listen to receive a message from our direct neighbour.
        /// </summary>
        /// <returns>The message received.</returns>
        private string ReceiveMessage()
        {
            byte[] byteBuffer = new byte[256];
            int bytesReceived = 0;
            string message = string.Empty;
            
            using (NetworkStream stream = new NetworkStream(this.client.Client))
            {
                do
                {
                    bytesReceived = stream.Read(byteBuffer, 0, byteBuffer.Length);
                    message += Encoding.ASCII.GetString(byteBuffer, 0, bytesReceived).Replace("\0", String.Empty); ;
                } while (stream.DataAvailable);
            }

            return message;
        }
        #endregion
        
        /// <summary>
        /// Release allocated resources
        /// </summary>
        public void Dispose()
        {
            if (!this.disposed)
            {
                this.disposed = true;
                this.client.Client.Close();
                this.client.Close();
            }
        }

        /// <summary>
        /// Our Neighbour's port number
        /// </summary>
        public int Node { get; set; }

        #region Factories
        /// <summary>
        /// Starts a new connection.
        /// Will try to connect with a server on this network.
        /// </summary>
        /// <param name="node">The node number to connect with.</param>
        /// <returns>An instance of a Client.</returns>
        public static Client Start(int node)
        {
            return new Client(node);
        }
        /// <summary>
        /// Creates a new incoming connection.
        /// Will try to handshake with the client and start receiving messages.
        /// </summary>
        /// <param name="socket">An existing connection accepted by the server.</param>
        /// <returns>An instance of a Client.</returns>
        public static Client Create(Socket socket)
        {
            return new Client(socket);
        }
        /// <summary>
        /// Fakes a local connection (no actual connection will be created).
        /// </summary>
        /// <returns>An instance of a Client.</returns>
        public static Client Local()
        {
            return new Client();
        }
        #endregion
    }
}
