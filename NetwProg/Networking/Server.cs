﻿using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace NetwProg.Networking
{
    /// <summary>
    /// Server class which will listen to localhost on this node's (port)number for incoming connections.
    /// </summary>
    class Server
    {
        private readonly TcpListener listener;

        /// <summary>
        /// Starts the server for this program.
        /// </summary>
        /// <returns>Returns an instance of this class</returns>
        public static Server Start()
        {
            return new Server();
        }

        private Server()
        {
            this.listener = new TcpListener(IPAddress.Loopback, NetwProg.LocalNode);
            this.listener.Start();

            new Thread(this.ListenLoop).Start();
        }

        private void ListenLoop()
        {
            Socket socket = null;
            while (true)
            {
                socket = this.listener.AcceptSocket();
                Client.Create(socket);
            }
        }
    }
}
