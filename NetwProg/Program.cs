﻿/*
 * NetChange Algorithm, Concurrency Assignment 2.
 * Copyright © 2014 Alper Aslan (3869768) & Alexandra Belzon (3634396), Utrecht University. All rights reserved.
 */

using System.Linq;

namespace NetwProg
{
    class Program
    {
        static void Main(string[] args)
        {
            var nodes = args
                .Select<string, int>(node => int.Parse(node));

            new NetwProg(nodes);
        }
    }
}