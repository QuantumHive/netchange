﻿/*
 * LET OP!
 * Code van Alper hergebruikt van vorig jaar (2013)
 * Voor dit jaar is de code merendeels herschreven:
 * - Veel functionaliteiten opgesplitst in meerdere classes.
 * - Code netter
 * - Beter geoptimaliseerd
 */

using NetwProg.Commands;
using NetwProg.NetChange;
using NetwProg.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace NetwProg
{
    /// <summary>
    /// This program acts as a node in a local network.
    /// The node can act as both server and client.
    /// Every node has a unique port number assigned to. This is also the port the node listens on.
    /// Initially node's with greater numbers than their neighbours act as servers.
    /// While node's with smaller numbers than their neighbours act as clients.
    /// Whenever we want to connect to another node we create one socket. Every node has a listener to accept these connections.
    /// As you can see in the sketch below, the client connects to the server through one socket.
    /// +----------+        +----------+
    /// |1000      |        |      1001|
    /// |  Connect ========== Listen   |
    /// |     |     [socket]     |     |
    /// | [Receive]==========[Receive] |
    /// |          |        |          |
    /// +----------+        +----------+
    /// For every node we are connected to, we fire a new thread that loops over incoming data (Receive).
    /// We also loop in the main thread for user input via the console application.
    /// This input is then treated as outgoing data and sent to the corresponding node through the socket.
    /// 
    /// The node keeps track of changes in the network and calculates the shortest distance to other nodes with the NetChange algorithm.
    /// A direct connection between two nodes is considered a distance of 1 hop.
    /// A-----B
    /// |\    |
    /// | \   |
    /// |  \  |
    /// |   \ |
    /// |    \|
    /// C     D
    /// Every node has a routing table where the shortest distances are stored for each destination node in the network.
    /// For example, node's B routing table will look like:
    /// 
    /// Destination   Distance   Neighbour
    /// B             0          local
    /// A             1          A
    /// D             1          D
    /// C             2          A
    /// 
    /// As you can see, the shortest distance from B to C is 2 hops, which passes A.
    /// Another path would be B -> D -> A -> C. This takes 3 hops and the algorithm will ignore this path.
    /// </summary>
    class NetwProg
    {
        //to synchronize threads that are recomputing (is used as a ref, so cannot be a property)
        public static int Recomputing;

        /// <summary>
        /// Start NetwProg.
        /// Will loop over console command line input.
        /// </summary>
        /// <param name="nodes">Initial known direct neighbours</param>
        public NetwProg(IEnumerable<int> nodes)
        {
            //init statics
            LocalNode = nodes.FirstOrDefault<int>();
            Recomputing = 0;
            Initializing = true;
            ConnectionCount = nodes.Count() - 1;
            MyLock = new { };
            NetChangeInstance = new NetChangeService();
            
            Console.Title = string.Format("NetChange {0}", LocalNode.ToString());

            NetChangeInstance.Network = nodes.ToList<int>();

            Server.Start(); //start listening

            //start connecting to nodes with bigger node numbers
            nodes
                .Where<int>(n => n > LocalNode)
                .ToList<int>()
                .ForEach(n => Client.Start(n));

            //wait for all connections to complete
            while (NetChangeInstance.Neighbours.Count < ConnectionCount) { Thread.Sleep(500); }

            NetChangeInstance.Init();

            //command line input
            while (CommandLineParser.ParseExecute(CommandOptions.Instance, Console.ReadLine())) ;
        }

        #region Properties
        /// <summary>
        /// NetChange implementation
        /// </summary>
        public static INetChange NetChangeInstance { get; set; }

        /// <summary>
        /// This node's/process' (port)number
        /// </summary>
        public static int LocalNode { get; set; }

        /// <summary>
        /// Prevents infinite wait after connection timeout occurs
        /// </summary>
        public static int ConnectionCount { get; set; }

        /// <summary>
        /// Receivers should wait untill NetChange's done initializing
        /// </summary>
        public static bool Initializing { get; set; }

        /// <summary>
        /// For locking
        /// </summary>
        public static Object MyLock { get; set; }
        #endregion
    }
}
