﻿using System;
using System.Text;

namespace NetwProg
{
    /// <summary>
    /// Custom Logger class with static methods to directly ouput logs.
    /// </summary>
    class Logger
    {
        private Logger() { }

        /// <summary>
        /// Should only be used to output exercise specific output.
        /// </summary>
        /// <param name="message">A composite format string.</param>
        /// <param name="args">The object to format.</param>
        public static void Output(string message, params object[] args)
        {
            Console.WriteLine(string.Format(message, args));
        }

        /// <summary>
        /// Debugging output.
        /// Will be ignored by TomJudge.
        /// </summary>
        /// <param name="message">A composite format string.</param>
        /// <param name="args">The object to format.</param>
        public static void Debug(string message, params object[] args)
        {
            string msg = string.Format(message, args);
            Console.WriteLine(string.Format("//DEBUG ({0}): {1}", DateTime.Now.ToLongTimeString(), msg));
        }

        /// <summary>
        /// Outputs errors with exception messages.
        /// Will be ignored by TomJudge.
        /// </summary>
        /// <param name="e">Represents errors that occur during application execution.</param>
        public static void Error(Exception e)
        {
            Error(e, string.Empty);
        }

        /// <summary>
        /// Outputs errors with exception messages and a custom message.
        /// Will be ignored by TomJudge.
        /// </summary>
        /// <param name="e">Represents errors that occur during application execution.</param>
        /// <param name="message">A composite format string.</param>
        /// <param name="args">The object to format.</param>
        public static void Error(Exception e, string message, params object[] args)
        {
            StringBuilder text = new StringBuilder();

            string msg = string.Format(message, args);
            text.AppendLine(string.Format("//ERROR ({0}): {1}", DateTime.Now, msg));
            text.AppendLine("//Exception:");
            text.AppendLine("//" + e.Message);
            text.AppendLine(Logger.InnerException(e.InnerException, 1));
            
            Console.WriteLine(text);
        }

        /// <summary>
        /// Internal method to output indented inner exception messages based on their inner levels.
        /// </summary>
        /// <param name="e">Represents errors that occur during application execution.</param>
        /// <param name="level">The indentation level.</param>
        /// <returns></returns>
        private static string InnerException(Exception e, int level)
        {
            StringBuilder text = new StringBuilder();
            if (e != null)
            {
                string indent = "";
                for (int i = 0; i < level; i++)
                {
                    indent += "\t";
                }
                text.AppendLine("//" + indent + "Inner exception:");
                text.AppendLine("//" + indent + e.Message);
                text.AppendLine(Logger.InnerException(e.InnerException, ++level));
            }
            return text.ToString();
        }
    }
}